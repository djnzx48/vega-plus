zeusemulate "48K"
zoLogicOperatorsHighPri = false

KEYBOARD        equ $02bf
NEW             equ $11b7

LAST_K          equ $5c08

FRAMES          equ 23672
PROG            equ 23635
RAMTOP          equ 23730
P_RAMT          equ 23732
RASP_PIP        equ 23608
UDG             equ 23675
CHARS           equ 23606
ERR_SP          equ 23613


org 64000
start:
int_table:
rept 257
    db 251
mend

org 64257
setup:
    ld ix, FRAMES
    ld a, (ix+0)
    ld (seed1), a
    ld a, (ix+1)
    ld (seed1+1), a
    ld (seed2), a
    ld a, (ix+2)
    ld (seed2+1), a
    
    di
    ld a, HIGH int_table
    ld i, a
    im 2

    ; do a NEW (copied from ROM)
    ld de, (RAMTOP)
    exx
    ld bc, (P_RAMT)
    ld de, (RASP_PIP)
    ld hl, (UDG)
    exx

    ld a, 7
    out ($fe), a
    ld a, $3f

RAM_CHECK:
    ld h, d
    ld l, e

RAM_FILL:
    ld (hl), 2
    dec hl
    cp h
    jr nz, RAM_FILL

RAM_READ:
    and a
    sbc hl, de
    add hl, de
    inc hl
    jr nc, RAM_DONE
    dec (hl)
    jr z, RAM_DONE
    dec (hl)
    jr z, RAM_READ

RAM_DONE:
    dec hl
    exx
    ld (P_RAMT), bc
    ld (RASP_PIP), de
    ld (UDG), hl
    exx

RAM_SET:
    ld (RAMTOP), hl
    ld hl, $3c00
    ld (CHARS), hl
    
    ld hl, (RAMTOP)
    ld (hl), $3e
    dec hl
    ld sp, hl
    dec hl
    dec hl
    ld (ERR_SP), hl
    
    ; jump back into the ROM
    jp $1230

org 64507 ; interrupt routine
interrupt:
    push af
    push hl
    ld hl, (FRAMES)
    inc hl
    ld (FRAMES), hl
    ld a, h
    or l
    jr nz, key_int
    inc (iy+$40)
key_int:
    push bc
    push de

    ; check for a keypress
    xor a
    in a, ($fe)
    cpl
    and %00011111
    jr nz, pressed_key

    xor a
    ld (key_on), a
    jr read_keys

pressed_key:
    ld a, (key_on)
    or a
    jr z, new_key

    ld a, (key_delay)
    or a
    jr z, read_keys
    dec a
    ld (key_delay), a
    jr skip_keys

new_key:
    ld a, 1
    ld (key_on), a

    ; calculate a random number
seed1 equ $ + 1
    ld hl, 0
    ld b, h
    ld c, l
    add hl, hl
    add hl, hl
    inc l
    add hl, bc
    ld (seed1), hl

seed2 equ $ + 1
    ld hl, 0
    add hl, hl
    sbc a, a
    and %00101101
    xor l
    ld l, a
    ld (seed2), hl
    add hl, bc

    ld a, (freeze_chance)
    ld e, a
    ld a, (freeze_chance+1)
    ld d, a
    sbc hl, de
    jr c, freeze

    ; chance for freezing increases each keypress
    ld hl, 50
    add hl, de
    ld (freeze_chance), hl

    ; reduce delay a bit
    sra l
    ld a, l
    sra a
    add a, l
    ld (key_delay), a
    jr skip_keys

read_keys:
    call KEYBOARD

skip_keys:
    pop de
    pop bc
    pop hl
    pop af
    ei
    reti

freeze:
    di

    ld hl, $5800
    ld bc, 768

    ; turn off FLASH attrs for screen
scrnloop:
    ld a, (hl)
    and 127
    ld (hl), a

    inc hl
    dec bc
    ld a, b
    or c
    jr nz, scrnloop

    halt

key_on:
    db 0
key_delay:
    db 0
freeze_chance:
    dw 0

end_marker:

Zeus_PC equ setup

output_bin "../bin/build/ir/game.bin", start, end_marker - start
